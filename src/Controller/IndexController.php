<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace Migrationpanel\Controller;

use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Migrations\Migrations;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class IndexController  extends AppController
{
    public $helpers = [
        'Migrationpanel.Migration'
    ];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
//        $this->Auth->allow(['index', 'up']);
    }

    public function index()
    {
        $migration = new Migrations();
        $migrations = [];
        $migrations['App'] = $migration->status();
        foreach (Plugin::loaded() as $plugin) {
            if (!$this->hasMigrations($plugin)) { continue; }

            $migrations[$plugin] = $migration->status(['plugin' => $plugin]);

        }
        $this->set('migrations', $migrations);
    }

    public function up($id, $plugin = null)
    {
        $options = ['target' => $id];
        if (!is_null($plugin)) {
            $options['plugin'] = str_replace('_', '/', $plugin);
        }

        $migrations = new Migrations();
        $migrations->migrate($options);
        $this->redirect('migrationpanel/index');
    }

    public function down($id, $plugin = null)
    {
        $options = ['target' => $id];
        if (!is_null($plugin)) {
            $options['plugin'] = str_replace('_', '/', $plugin);
        }

        $migrations = new Migrations();
        $migrations->rollback($options);
        $this->redirect('migrationpanel/index');
    }

    private function hasMigrations($plugin)
    {
        $path = Plugin::path($plugin) . 'config' . DS . 'Migrations' . DS;
        return file_exists($path);
    }
}
