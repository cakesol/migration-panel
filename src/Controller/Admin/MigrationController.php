<?php

namespace Cakesol\Migrationpanel;

use Migrations\Migrations;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 */
class MigrationController extends AppController
{

    /**
     * Displays a view
     *
     */
    public function display()
    {
        $migrations = new Migrations();
        $this->set('migrations', $migrations->status());
    }

    public function rollback()
    {

    }

    public function up()
    {
        
    }
}
