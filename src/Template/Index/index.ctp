<table>
    <thead>
    <td>Plugin</td>
    <td>Status</td>
    <td>id</td>
    <td>Name</td>
    <td>Actions</td>
    </thead>
    <tbody>
    <?php foreach ($migrations as $plugin => $migration_results) :?>
        <?php $previousTarget = 0;?>
        <?php foreach ($migration_results as $migration) :?>
            <?php $migration['previousTarget'] = $previousTarget;?>

            <tr>
                <td><?php echo $plugin;?></td>
                <td><?php echo $migration['status'];?></td>
                <td><?php echo $migration['id'];?></td>
                <td><?php echo $migration['name'];?></td>
                <td><?php echo $this->Migration->upDowngradeUrl($migration, $plugin);?></td>
            </tr>

            <?php $previousTarget = $migration['id'];?>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>

