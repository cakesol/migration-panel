Migration-panel plugin for CakePHP
================================

Requirements
------------
* CakePHP 3.5.0+
* PHP 7.0+
* Cakephp/Migrations 1.0+

Installation
------------
You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).
The recommended way to install composer packages is:

```
composer require cakesol/migration-panel
```

or update your composer.json file as below
```
 "require": {
        "cakesol/migration-panel": "dev-master"
    },
```
Enable the plugin in your application bootstrap.php
```
Plugin::load('Migrationpanel', ['bootstrap' => true, 'routes' => true]);
```

Support
-------

Please report bugs and feature request in the [issues](https://gitlab.com/cakesol/migration-panel/issues) section of this repository.


License
-------

Copyright 2018 Solutia. All rights reserved.

Licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php) License. Redistributions of the source code included in this repository must retain the copyright notice found in each file.